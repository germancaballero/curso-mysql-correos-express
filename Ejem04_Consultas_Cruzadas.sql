USE bd_correosexpress;
-- Sin condiciones: Devuelve tooooodo cruzado, combinados todos los registros: 8 x 4 = 32 reg.
 SELECT * FROM envio_carta, cp;
-- Con la única condicion de que los IDs del CP coincidan, devuelte todos los envios_carta 
-- (por ser el primer campo) con su respectivo CP
 SELECT * FROM envio_carta, cp WHERE envio_carta.CP_id = cp.id;
-- Aquí lo mismo pero el orden de los campos cambia
-- SELECT * FROM cp, envio_carta WHERE envio_carta.CP_id = cp.id;
-- Buscamos envios por CP, de Rivas, pero sólo devolvemos los campos de una tabla:
-- SELECT envio_carta.* FROM envio_carta, cp  WHERE envio_carta.CP_id = cp.id AND cp.CP = 28331;
-- SELECT envio_carta.* FROM envio_carta, cp  WHERE envio_carta.CP_id = cp.id AND cp.nombre = 'Alcobendas'; 
    -- Like y otros coparadores se pueden combinar con todo: Consultas a varias tablas, etc... En general, hay infinitud de combinaciones
-- SELECT envio_carta.* FROM envio_carta, cp  WHERE envio_carta.CP_id = cp.id AND cp.nombre LIKE '%I%'; 
-- Mostrando sólo campos de una tabla
-- SELECT envio_carta.* FROM envio_carta, cp WHERE envio_carta.CP_id = cp.id AND cp.nombre LIKE '%I%'; 
-- Mostramos sólo ciertos campos:
--  SELECT nombre FROM provincia;
-- Sólo el nombre de los Códigos postales de una provincia 'Barcelona'
-- SELECT cp.nombre FROM cp, provincia WHERE cp.provincia_id = provincia.id AND provincia.nombre = 'Barcelona';
-- Cruzar 3 tablas: P. ej. Mostrar todos los envíos de la provincia Madrid
/*SELECT envio_carta.remitente, envio_carta.destinatario, envio_carta.direcc_entrega
	FROM envio_carta, cp, provincia
	WHERE cp.provincia_id = provincia.id 
	  AND envio_carta.cp_id = cp.id
      AND provincia.nombre = 'Madrid';
-- Cuando hagamos consultas largas, podemos usar los ALIAS, con la palabra AS
SELECT ec.remitente AS rem, ec.destinatario AS dest, ec.direcc_entrega
	FROM envio_carta AS ec, cp AS c, provincia AS p
	WHERE c.provincia_id = p.id AND ec.cp_id = c.id AND p.nombre = 'Madrid';
    
-- Consulta a 4 tablas: Los usuarios xiste en BD) que han enviado cartas a Barcelona

SELECT U.nombre
	FROM usuario AS u, envio_carta AS ec, CP AS c, provincia AS p
	WHERE u.id = ec.remitente_id
      AND c.provincia_id = p.id 
      AND ec.cp_id = c.id 
      AND p.nombre = 'Barcelona';
      */
