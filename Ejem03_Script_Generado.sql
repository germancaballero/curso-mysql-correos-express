-- MySQL Script generated by MySQL Workbench
-- Fri Oct  9 19:30:06 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema bd_correosexpress
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `bd_correosexpress` ;

-- -----------------------------------------------------
-- Schema bd_correosexpress
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `bd_correosexpress` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci ;
USE `bd_correosexpress` ;

-- -----------------------------------------------------
-- Table `bd_correosexpress`.`usuario`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bd_correosexpress`.`usuario` ;

CREATE TABLE IF NOT EXISTS `bd_correosexpress`.`usuario` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(320) NOT NULL,
  `nombre` VARCHAR(50) NOT NULL,
  `direccion` VARCHAR(100) NULL DEFAULT NULL,
  `edad` TINYINT UNSIGNED NULL DEFAULT NULL,
  `premium` BIT(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 1
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `bd_correosexpress`.`provincia`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bd_correosexpress`.`provincia` ;

CREATE TABLE IF NOT EXISTS `bd_correosexpress`.`provincia` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(30) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_correosexpress`.`cp`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bd_correosexpress`.`cp` ;

CREATE TABLE IF NOT EXISTS `bd_correosexpress`.`cp` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cp` SMALLINT UNSIGNED NOT NULL,
  `nombre` VARCHAR(50) NULL,
  `provincia_id` INT UNSIGNED NOT NULL,
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  PRIMARY KEY (`id`, `provincia_id`),
  UNIQUE INDEX `CP_UNIQUE` (`cp` ASC) VISIBLE,
  INDEX `fk_CP_provincia1_idx` (`provincia_id` ASC) VISIBLE,
  CONSTRAINT `fk_CP_provincia1`
    FOREIGN KEY (`provincia_id`)
    REFERENCES `bd_correosexpress`.`provincia` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_correosexpress`.`envio_carta`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bd_correosexpress`.`envio_carta` ;

CREATE TABLE IF NOT EXISTS `bd_correosexpress`.`envio_carta` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `remitente` VARCHAR(50) NULL,
  `destinatario` VARCHAR(50) NULL,
  `direcc_entrega` VARCHAR(100) NULL DEFAULT NULL,
  `remitente_id` INT UNSIGNED NULL DEFAULT NULL,
  `destinatario_id` INT UNSIGNED NULL DEFAULT NULL,
  `CP_id` INT UNSIGNED NOT NULL,
  `fecha` DATETIME NULL DEFAULT NOW( ),
  PRIMARY KEY (`id`, `CP_id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  INDEX `fk_usu_remit_idx` (`remitente_id` ASC) VISIBLE,
  INDEX `fk_usu_dest_idx` (`destinatario_id` ASC) VISIBLE,
  INDEX `fk_envio_carta_CP1_idx` (`CP_id` ASC) VISIBLE,
  CONSTRAINT `fk_usu_dest`
    FOREIGN KEY (`destinatario_id`)
    REFERENCES `bd_correosexpress`.`usuario` (`id`),
  CONSTRAINT `fk_usu_remit`
    FOREIGN KEY (`remitente_id`)
    REFERENCES `bd_correosexpress`.`usuario` (`id`),
  CONSTRAINT `fk_envio_carta_CP1`
    FOREIGN KEY (`CP_id`)
    REFERENCES `bd_correosexpress`.`cp` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


-- -----------------------------------------------------
-- Table `bd_correosexpress`.`departamento`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bd_correosexpress`.`departamento` ;

CREATE TABLE IF NOT EXISTS `bd_correosexpress`.`departamento` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC) VISIBLE,
  UNIQUE INDEX `nombre_UNIQUE` (`nombre` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `bd_correosexpress`.`rol`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `bd_correosexpress`.`rol` ;

CREATE TABLE IF NOT EXISTS `bd_correosexpress`.`rol` (
  `usuario_id` INT UNSIGNED NOT NULL,
  `departamento_id` INT UNSIGNED NOT NULL,
  `categoria` VARCHAR(50) NOT NULL,
  PRIMARY KEY (`usuario_id`, `departamento_id`),
  INDEX `fk_usuario_has_departamento_departamento1_idx` (`departamento_id` ASC) VISIBLE,
  INDEX `fk_usuario_has_departamento_usuario1_idx` (`usuario_id` ASC) VISIBLE,
  CONSTRAINT `fk_usuario_has_departamento_usuario1`
    FOREIGN KEY (`usuario_id`)
    REFERENCES `bd_correosexpress`.`usuario` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuario_has_departamento_departamento1`
    FOREIGN KEY (`departamento_id`)
    REFERENCES `bd_correosexpress`.`departamento` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8mb4
COLLATE = utf8mb4_0900_ai_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `bd_correosexpress`.`usuario`
-- -----------------------------------------------------
START TRANSACTION;
USE `bd_correosexpress`;
INSERT INTO `bd_correosexpress`.`usuario` (`id`, `email`, `nombre`, `direccion`, `edad`, `premium`) VALUES (DEFAULT, 'ful@email.es', 'Fulano', 'C / Pantomima 10', 22, 0);
INSERT INTO `bd_correosexpress`.`usuario` (`id`, `email`, `nombre`, `direccion`, `edad`, `premium`) VALUES (DEFAULT, 'men@email.es', 'Mengano', 'C/Alli 30', 30, 1);
INSERT INTO `bd_correosexpress`.`usuario` (`id`, `email`, `nombre`, `direccion`, `edad`, `premium`) VALUES (DEFAULT, 'fulanita@aa.es', 'Fulanita', 'C/Pez 10', 33, 1);
INSERT INTO `bd_correosexpress`.`usuario` (`id`, `email`, `nombre`, `direccion`, `edad`, `premium`) VALUES (DEFAULT, 'pedro@aa.es', 'Pedro', 'C/Pan 6', 40, 1);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bd_correosexpress`.`provincia`
-- -----------------------------------------------------
START TRANSACTION;
USE `bd_correosexpress`;
INSERT INTO `bd_correosexpress`.`provincia` (`id`, `nombre`) VALUES (DEFAULT, 'Barcelona');
INSERT INTO `bd_correosexpress`.`provincia` (`id`, `nombre`) VALUES (DEFAULT, 'Madrid');

COMMIT;


-- -----------------------------------------------------
-- Data for table `bd_correosexpress`.`cp`
-- -----------------------------------------------------
START TRANSACTION;
USE `bd_correosexpress`;
INSERT INTO `bd_correosexpress`.`cp` (`id`, `cp`, `nombre`, `provincia_id`) VALUES (DEFAULT, 08001, 'Barcelona', 1);
INSERT INTO `bd_correosexpress`.`cp` (`id`, `cp`, `nombre`, `provincia_id`) VALUES (DEFAULT, 08902, 'L\'Hospitalet de Llobregat', 1);
INSERT INTO `bd_correosexpress`.`cp` (`id`, `cp`, `nombre`, `provincia_id`) VALUES (DEFAULT, 28331, 'Rivas Vaciamadrid', 2);
INSERT INTO `bd_correosexpress`.`cp` (`id`, `cp`, `nombre`, `provincia_id`) VALUES (DEFAULT, 28100, 'Alcobendas', 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bd_correosexpress`.`envio_carta`
-- -----------------------------------------------------
START TRANSACTION;
USE `bd_correosexpress`;
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`) VALUES (DEFAULT, NULL, NULL, NULL, 2, 1, 2);
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`) VALUES (DEFAULT, NULL, NULL, NULL, 3, 1, 2);
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`, `fecha`) VALUES (DEFAULT, 'Zulano', 'Guzmán', 'C/Goma 77', NULL, NULL, 4, '2020-10-09 19:32:35');
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`, `fecha`) VALUES (DEFAULT, NULL, 'Zulano', 'C/Zumo 30', 1, NULL, 3, '2020-11-09 19:32:35');
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`, `fecha`) VALUES (DEFAULT, 'Guzmán', NULL, NULL, NULL, 3, 3, NULL);
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`, `fecha`) VALUES (DEFAULT, 'Guzmán', NULL, NULL, NULL, 2, 1, NULL);
INSERT INTO `bd_correosexpress`.`envio_carta` (`id`, `remitente`, `destinatario`, `direcc_entrega`, `remitente_id`, `destinatario_id`, `CP_id`, `fecha`) VALUES (DEFAULT, NULL, 'Zulano', 'C/Zumo 30', 2, NULL, 3, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `bd_correosexpress`.`departamento`
-- -----------------------------------------------------
START TRANSACTION;
USE `bd_correosexpress`;
INSERT INTO `bd_correosexpress`.`departamento` (`id`, `nombre`) VALUES (DEFAULT, 'Comercial');
INSERT INTO `bd_correosexpress`.`departamento` (`id`, `nombre`) VALUES (DEFAULT, 'Desarrollo');
INSERT INTO `bd_correosexpress`.`departamento` (`id`, `nombre`) VALUES (DEFAULT, 'Administración');
INSERT INTO `bd_correosexpress`.`departamento` (`id`, `nombre`) VALUES (DEFAULT, 'Clientes');

COMMIT;


-- -----------------------------------------------------
-- Data for table `bd_correosexpress`.`rol`
-- -----------------------------------------------------
START TRANSACTION;
USE `bd_correosexpress`;
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (1, 4, 'Cliente');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (2, 4, 'Cliente');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (3, 4, 'Cliente');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (1, 1, 'Comercial');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (2, 2, 'Friki');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (3, 1, 'Comercial');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (3, 2, 'Programadora');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (3, 3, 'Administradora');
INSERT INTO `bd_correosexpress`.`rol` (`usuario_id`, `departamento_id`, `categoria`) VALUES (4, 4, 'Cliente');

COMMIT;

