USE `bd_correosexpress`;
CREATE  OR REPLACE VIEW `cantidad_envio_por_dept` AS (SELECT d.nombre, count(ec.id) AS cantidad_envios
	FROM envio_carta AS ec 
	INNER JOIN usuario 		AS u ON u.id = ec.remitente_id
	INNER JOIN rol 			AS r ON u.id = r.usuario_id
	INNER JOIN departamento AS d ON d.id = r.departamento_id
	GROUP BY d.id);

SELECT avg(cantidad_envios) FROM bd_correosexpress.cantidad_envio_por_prov;
