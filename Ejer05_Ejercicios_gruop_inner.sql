-- select * from usuario, envio_carta WHERE usuario.id = envio_carta.remitente_id  OR usuario.id = envio_carta.destinatario_id  group by(envio_carta.id) ;
-- SELECT * from usuario INNER JOIN envio_carta ON usuario.id = envio_carta.destinatario_id;

-- 4) Retorna los códigos postales que haya usado Fulano en sus envíos
/*SELECT cp.* FROM usuario AS u, envio_carta AS ec, cp 
WHERE            u.id = ec.remitente_id
  AND        ec.cp_id = cp.id 
  AND        u.nombre = 'Fulano';
SELECT * FROM cp
	INNER JOIN envio_carta AS ec ON  ec.cp_id = cp.id 
    INNER JOIN usuario AS u ON u.id = ec.remitente_id
    WHERE u.nombre = 'Fulano';    
    */
-- 5) ¿Qué codigos postales han recibido cartas de alguien con un email cuyo dominio es '@email.es'?
/*SELECT cp.* FROM usuario AS u, envio_carta AS ec, cp 
WHERE            u.id = ec.remitente_id
  AND        ec.cp_id = cp.id 
  AND       u.email LIKE '%@email.es'
  GROUP BY (cp.id) ORDER BY (cp.id) ASC;
SELECT cp.* FROM usuario AS u
	INNER JOIN envio_carta AS ec 		ON u.id = ec.remitente_id
	INNER JOIN cp  						ON ec.cp_id = cp.id 
	WHERE  u.email LIKE '%@email.es' 	GROUP BY (cp.id) ORDER BY (cp.id) ASC;  
    */
-- ¿Cuantos envios se han hecho por provincia?
/*SELECT p.Nombre, COUNT(*) FROM envio_carta AS ec, cp, provincia AS p
	WHERE        ec.cp_id = cp.id 
	  AND cp.provincia_id = p.id
	GROUP BY (p.id);*/

SELECT p.Nombre, COUNT(*) 		FROM envio_carta AS ec
	INNER JOIN cp   			ON ec.cp_id = cp.id 
	INNER JOIN provincia AS p   ON cp.provincia_id = p.id
		GROUP BY (p.id);

-- 3) Devuelve los usuarios que hayan ENVIADO cartas a un código postal catalán
/*SELECT u.* FROM usuario AS u, envio_carta AS ec, cp, provincia AS p 
WHERE 		     u.id = ec.remitente_id
  AND        ec.cp_id = cp.id 
  AND cp.provincia_id = p.id
  AND        p.nombre = 'Barcelona';*/
    
-- Cuantos envíos se han hecho por cada departamento
SELECT d.nombre, count(ec.id) AS cantidad_envios
FROM envio_carta AS ec 
INNER JOIN usuario 		AS u ON u.id = ec.remitente_id
INNER JOIN rol 			AS r ON u.id = r.usuario_id
INNER JOIN departamento AS d ON d.id = r.departamento_id
GROUP BY d.id;

