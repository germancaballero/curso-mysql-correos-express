-- Contar todos los usuarios que hay (4)
SELECT COUNT(id) FROM Usuario;

-- Hacer una consulta indicando cuantos usuarios tiene cada departamento
SELECT departamento.nombre, COUNT(rol.departamento_id) FROM rol, departamento 
WHERE rol.departamento_id = departamento.id 
GROUP BY rol.departamento_id;
