-- Al igual nos sirve para condiciones sólo en campos, no nos sirve para funciones agregadas (como COUNT)
-- Ejemplo:
-- Cuantos envíos se han hecho por cada departamento sólo en los que se hayan hecho más de 1
/*SELECT d.nombre, COUNT(ec.id) AS cantidad_envios
FROM envio_carta AS ec 
INNER JOIN usuario 		AS u ON u.id = ec.remitente_id
INNER JOIN rol 			AS r ON u.id = r.usuario_id
INNER JOIN departamento AS d ON d.id = r.departamento_id
GROUP BY d.id
HAVING COUNT(ec.id) > 1;*/


-- ¿Cual es la media de envios de todos los departamentos?
/*SELECT AVG(cantidad_envios) FROM (SELECT COUNT(ec.id) AS cantidad_envios
FROM envio_carta AS ec 
INNER JOIN usuario 		AS u ON u.id = ec.remitente_id
INNER JOIN rol 			AS r ON u.id = r.usuario_id
INNER JOIN departamento AS d ON d.id = r.departamento_id
GROUP BY d.id) AS envios_por_dept;*/

-- ¿Cual es la media de envíos por usuario?
SELECT AVG(cant_env) FROM (
	SELECT COUNT(ec.id) AS cant_env FROM envio_carta AS ec 
		INNER JOIN usuario AS u ON u.id = ec.remitente_id
		GROUP BY u.id) AS envios_por_u;
        
